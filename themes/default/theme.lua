---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local lfs = require("lfs")

local gears = require("gears")
local awful = require("awful")

local wibox = require("wibox")

-- using eminent for dynamic tagging from https://github.com/guotsuan/eminent
require("eminent.eminent")

local lain = require("lain")

local theme = {}

theme.font          = "Terminus 8"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#c54243"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(4)
theme.border_width  = dpi(2)
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"
theme.gap_single_client = true -- false
-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"
theme.taglist_bg_focus = theme.bg_focus -- "#ff9933"
--theme.taglist_fg_focus = "#000000"

theme.tooltip_fg = theme.fg_focus
theme.tooltip_bg = theme.bg_focus

theme.tasklist_bg_focus = theme.bg_normal
theme.tasklist_fg_focus = "#d3d3d3" -- theme.taglist_bg_focus
theme.tasklist_bg_urgent = theme.bg_normal
theme.tasklist_fg_urgent = theme.bg_urgent

theme.tasklist_disable_icon = true

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(24)
theme.menu_width  = dpi(150)
theme.menu_border_width  = dpi(0)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

--theme.wallpaper = themes_path.."default/background.png"
local wallpapers_path = os.getenv("HOME") .. "/Pictures/wallpapers"
function random_wallpaper(path)
    local wallpapers = {}
    for file in lfs.dir(path) do
        if string.match(file, ".*%.jpg") then
            table.insert(wallpapers, file)
        end
    end
    math.randomseed(os.time())
    return path .. "/" .. wallpapers[math.random(#wallpapers)]
end
theme.wallpaper = random_wallpaper(wallpapers_path)

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Papirus-Dark"

-- my customizations
theme.notification_icon_size = 64
-- theme.awesome_icon = nil
theme.maximized_hide_border = true

-- Wibox and widgets config
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- My widgets
local markup = lain.util.markup
local separators = lain.util.separators
local arrl_dl = separators.arrow_left("alpha", theme.taglist_bg_focus)
local arrl_ld = separators.arrow_left(theme.taglist_bg_focus, "alpha")

-- weather
-- Santiago, CL = 3871336
local myweather = lain.widget.weather({
      city_id = 3871336,
      notification_preset = { font = theme.font, fg = theme.fg_normal },
      weather_na_markup = lain.util.markup.fontfg(theme.font, theme.fg_normal, "N/A "),
      settings = function()
         local descr = weather_now["weather"][1]["description"]:lower()
         local units = math.floor(weather_now["main"]["temp"])
         widget:set_markup(lain.util.markup.fontfg(theme.font, theme.fg_normal, units .. "°C"))
      end
})

-- battery info from freedesktop upower
local mybattery_t = awful.tooltip {
    font = theme.font,
    bg = theme.tooltip_bg,
    fg = theme.tooltip_fg
}

local baticon = wibox.widget.imagebox()
local mybattery = awful.widget.watch(
    { awful.util.shell, "-c", "upower -i /org/freedesktop/UPower/devices/battery_BAT0 | sed -n '/present/,/icon-name/p'" },
    15,
    function(widget, stdout)
        local bat_now = {
            present      = "N/A",
            state        = "N/A",
            warninglevel = "N/A",
            energy       = "N/A",
            energyfull   = "N/A",
            energyrate   = "N/A",
            voltage      = "N/A",
            percentage   = "N/A",
            capacity     = "N/A",
            icon         = "N/A"
        }

        for k, v in string.gmatch(stdout, '([%a]+[%a|-]*):%s*\'?([%a|%d]+[,|%a|%d|-]*)') do
            if     k == "present"       then bat_now.present      = v
            elseif k == "state"         then bat_now.state        = v
            elseif k == "warning-level" then bat_now.warninglevel = v
            elseif k == "energy"        then bat_now.energy       = string.gsub(v, ",", ".") -- Wh
            elseif k == "energy-full"   then bat_now.energyfull   = string.gsub(v, ",", ".") -- Wh
            elseif k == "energy-rate"   then bat_now.energyrate   = string.gsub(v, ",", ".") -- W
            elseif k == "voltage"       then bat_now.voltage      = string.gsub(v, ",", ".") -- V
            elseif k == "percentage"    then bat_now.percentage   = tonumber(v)              -- %
            elseif k == "capacity"      then bat_now.capacity     = string.gsub(v, ",", ".") -- %
            elseif k == "icon-name"     then bat_now.icon         = v
            end
        end

        -- customize here
        -- widget:set_text("Bat: " .. bat_now.percentage .. "% " .. bat_now.state .. " | icon: " .. bat_now.icon)
        widget:set_markup(markup.font(theme.font, bat_now.percentage .. "%" ))
        baticon:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/" .. bat_now.icon .. ".svg")
        mybattery_t:set_text(bat_now.state)
    end
)
mybattery_t:add_to_object(baticon)
mybattery_t:add_to_object(mybattery)


-- ALSA
local volicon = wibox.widget.imagebox()
local volume = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" or tonumber(volume_now.level) == 0 then
            volicon:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/audio-volume-muted-symbolic.svg")
        elseif tonumber(volume_now.level) > 0 and tonumber(volume_now.level) < 20 then
            volicon:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/audio-volume-low-zero-panel.svg")
        elseif tonumber(volume_now.level) >= 20 and tonumber(volume_now.level) < 80 then
            volicon:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/audio-volume-medium-symbolic.svg")
        else
            volicon:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/audio-volume-high-symbolic.svg")
        end

        widget:set_markup(markup.font(theme.font, " " .. volume_now.level .. "%"))
    end
})
volume.widget:buttons(awful.util.table.join(
    awful.button({}, 1, function() -- left click
        -- awful.spawn(string.format("%s -e alsamixer", terminal))
        awful.spawn("pavucontrol")
    end),
    awful.button({}, 2, function() -- middle click
        os.execute(string.format("%s set %s 100%%", volume.cmd, volume.channel))
        volume.update()
    end),
    awful.button({}, 3, function() -- right click
        os.execute(string.format("%s set %s toggle", volume.cmd, volume.togglechannel or volume.channel))
        volume.update()
    end),
    awful.button({}, 4, function() -- scroll up
        os.execute(string.format("%s set %s 1%%+", volume.cmd, volume.channel))
        volume.update()
    end),
    awful.button({}, 5, function() -- scroll down
        os.execute(string.format("%s set %s 1%%-", volume.cmd, volume.channel))
        volume.update()
    end)
))

-- capslock widget
theme.capslockicon = wibox.widget.imagebox("/usr/share/icons/Papirus-Dark/16x16/panel/capslock-off.svg")
theme.capslockicon:connect_signal("update_capslock",
                            function(w)
                               awful.spawn.with_line_callback(
                                  "bash -c 'sleep 0.2 && xset q'" ,
                                  {
                                     stdout = function (line)
                                        if line:match("Caps Lock") then
                                           local status = line:gsub(".*(Caps Lock:%s+)(%a+).*", "%2")
                                           w:set_image("/usr/share/icons/Papirus-Dark/16x16/panel/capslock-" .. status .. ".svg")
                                        end
                                     end
                               })
                            end
)

-- mpris info widget
local myplayer = wibox.widget { font = theme.font, widget = wibox.widget.textbox }
awful.spawn.with_line_callback(
   "playerctl --follow metadata --format '<{{status}}> {{artist}} - {{album}} - {{title}} '",
   {
      stdout = function (line)
         myplayer:set_text(line:gsub('<Playing>', ''):gsub('<(.+)>', '(%1)'))
      end
})
myplayer:buttons(awful.util.table.join(
  awful.button({ }, 1, function()
    awful.util.spawn_with_shell("playerctl play-pause")
  end)
))

-- Net
local neticon = wibox.widget.imagebox("/usr/share/icons/Papirus-Dark/16x16/panel/network-transmit-receive.svg")
local net = lain.widget.net({
      settings = function()
         widget:set_markup(markup.fontfg(theme.font, "#aaaaaa", " " .. net_now.received .. " ↓↑ " .. net_now.sent .. " "))
      end
})
---------------------------------
-- end of my widgets

local function set_wallpaper(s)
    -- Wallpaper
    if theme.wallpaper then
        local wallpaper = theme.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, false)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

my_tags = {
   names = {
      "term",
      "web",
      "im",
      "dev",
      "mm",
      "emacs",
      "doc",
      "misc",
      "fs"
   },
   layout = {
      awful.layout.suit.tile.right,
      awful.layout.suit.tile.right,
      awful.layout.suit.tile.right,
      awful.layout.suit.tile.right,
      awful.layout.suit.tile.right,
      awful.layout.suit.max,
      awful.layout.suit.max,
      awful.layout.suit.tile.right,
      awful.layout.suit.tile.left,
   }
}

function theme.at_screen_connect(s)
    -- taglist and tasklist buttons work weird if declared on theme.lua *and*
    -- outside this function
    -- When they were on rc.lua file these worked perfectly, but here,
    -- if declared outside this function then clients are changing their current
    -- screen. If a client was focus on screen 1, and then click on taglist on
    -- screen 2, that focused client switches to screen 2 on that tag, just like
    -- if used button 1 + modkey: the client was calling move_to_tag function.
    -- I don't know why.
    -- I will let these functions here until I discover why.

    local taglist_buttons = gears.table.join(
        awful.button({ }, 1, function(t) t:view_only() end),
        awful.button({ modkey }, 1, function(t)
                if client.focus then
                    client.focus:move_to_tag(t)
                end
        end),
        awful.button({ }, 3, awful.tag.viewtoggle),
        awful.button({ modkey }, 3, function(t)
                if client.focus then
                    client.focus:toggle_tag(t)
                end
        end),
        awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
        awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
    )

    local tasklist_buttons = gears.table.join(
        awful.button({ }, 1, function (c)
                if c == client.focus then
                    c.minimized = true
                else
                    c:emit_signal(
                        "request::activate",
                        "tasklist",
                        {raise = true}
                    )
                end
        end),
        awful.button({ }, 3, function()
                awful.menu.client_list({ theme = { width = 250 } })
        end),
        awful.button({ }, 4, function ()
                awful.client.focus.byidx(1)
        end),
        awful.button({ }, 5, function ()
                awful.client.focus.byidx(-1)
    end))

    -- Create a popup calendar popup for each screen
    s.mytextclock = wibox.widget.textclock()
    -- s.mytextclock.format = "<span color='#000000'> %a %b %d, %H:%M </span>"
    s.month_calendar = awful.widget.calendar_popup.month()
    s.month_calendar:attach( s.mytextclock, "tr" )
    s.month_calendar:call_calendar(0, "tr", s)

    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    -- awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    awful.tag(my_tags.names, s, my_tags.layout)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                              --awful.button({ }, 1, function () awful.layout.inc( 1) end),
                              --awful.button({ }, 3, function () awful.layout.inc(-1) end),
                              awful.button({ }, 4, function () awful.layout.inc( 1) end),
                              awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            myplayer,
            -- mykeyboardlayout,
            wibox.widget.systray(),
            neticon,
            net.widget,
            arrl_dl, --separators.arrow_left("alpha", "#000"),
            wibox.container.background(
                wibox.container.margin(
                    wibox.widget {
                        theme.capslockicon,
                        layout = wibox.layout.align.horizontal
                    }, 3, 3),
                theme.taglist_bg_focus),
            arrl_ld, --separators.arrow_left("#000", "alpha"),
            --wibox.container.background(
            wibox.container.margin(wibox.widget { volicon, volume, layout = wibox.layout.align.horizontal }, 6, 6),
            --"alpha"),
            arrl_dl, --separators.arrow_left("alpha", "#000"),
            wibox.container.background(
                wibox.container.margin(wibox.widget { baticon, mybattery, layout = wibox.layout.align.horizontal }, 6, 6),
                theme.taglist_bg_focus),
            arrl_ld, --separators.arrow_left("alpha", "#000"),
            wibox.container.margin(wibox.widget { myweather.icon, myweather.widget, layout = wibox.layout.align.horizontal }, 6, 3),
            wibox.container.background(wibox.container.margin(wibox.widget { s.mytextclock, layout = wibox.layout.align.horizontal }, 0, 3), "alpha"),
            s.mylayoutbox,
        },
    }
end
-- end of wibox and widgets config

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
